const beerContainer = document.querySelector('.mcontent');
const beerContainerItem = document.querySelector('.mcontent__item');
const request = new XMLHttpRequest();

request.open("GET", "https://api.punkapi.com/v2/beers?page=1&per_page=80", true);
request.onload = function () {
    const data = JSON.parse(this.response);
    if (request.status >= 200 && request.status < 400) {
        data.forEach(beers => {
            beerContainerItem.innerHTML +=
                `<li class="post">
                    <span class="title"><h2>${beers.name}</h2></span><img src="${beers.image_url}" /><br/><span class="tagline">${beers.tagline}</span><div>Contributed by: <strong>${beers.contributed_by}</strong><br/><p>Date: <strong>${beers.first_brewed}</strong></p></div>
                 </li>
                 <div class="popup">
                  <div class="popup_content">
                        <span class="closePopup fas fa-times"></span>
                        <span class="beerName">${beers.name}</span>
                        <div class="beerContent">
                            <div class="left">
                                <img src="${beers.image_url}" />
                            </div>
                            <div class="right">
                            <span class="beerDesc">${beers.description}</span>
                            <strong>Beer stats:</strong>
                            <ul class="beerStats">
                                <li><strong>ABV:</strong> ${beers.abv} %</li>
                                <li><strong>IBU:</strong> ${beers.ibu}</li>
                                <li><strong>EBC:</strong> ${beers.ebc}</li>
                                <li><strong>SRM:</strong> ${beers.srm}</li>
                            </ul>
                            <strong class="bestWithHead">Best width:</strong>
                            <ul class="foodParing">
                                ${beers.food_pairing.map(ingredient => `<li> - ${ingredient}</li>`).join("")}
                            </ul>
                            <strong class="brewersTipsHead">Brewers tips:</strong><p class="brewersTips">${beers.brewers_tips}</p>
                            <strong class="yeastHead">Yeast</strong>
                            <span>${beers.ingredients.yeast}</span>
                            </div>
                        </div>
                    </div>
                    </div>
                `;
        });
    } else {
        console.log('error');
    }
}
request.send();