var input = document.querySelector('.searchTerm');
var items = document.querySelector('.mcontent__item').getElementsByTagName('li');

input.addEventListener('keyup', function(ev) {
  var text = ev.target.value;
  var pat = new RegExp(text, 'i');
  for (let i=0; i < items.length; i++) {
    var item = items[i];
    if (pat.test(item.innerText)) {
     /* item.className = item.className.replace(/\s+?hidden/,'');*/
     item.classList.remove('hidden');
     console.log(item);
    }
    else {
      /*item.className = item.className + ' hidden';*/
       item.classList.add('hidden');
       console.log(item);
    }
  }
});

const starOnBeer = document.querySelectorAll('.fa-star');
const newStarOnBer = Array.from(starOnBeer);

newStarOnBer.forEach(star =>{
    star.addEventListener('click',function(){
        star.classList.add('coloredStar');
    });
});